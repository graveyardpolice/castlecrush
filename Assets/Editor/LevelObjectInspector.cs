﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelMonoObject))]
public class LevelObjectInspector : Editor {

    SerializedProperty materialProp;
    private int matIndex;
    private string[] matOptions;
    SerializedProperty randomFactorProp;
    SerializedProperty posProp;
    SerializedProperty scaleProp;
    SerializedProperty rotProp;
    SerializedProperty objNameProp;
    LevelMonoObject go; 
    

    void OnEnable()
    {
        go = (LevelMonoObject)target;
        materialProp = serializedObject.FindProperty("objectMaterial");
        matOptions = materialProp.enumDisplayNames;
        randomFactorProp = serializedObject.FindProperty("randomizeFactor");
        posProp = serializedObject.FindProperty("position");
        scaleProp = serializedObject.FindProperty("scale");
        rotProp = serializedObject.FindProperty("rot");
        objNameProp = serializedObject.FindProperty("prefabName");
    }

    public override void OnInspectorGUI()
    {
        //update our properties - must be done at beginning of OnInspectorGUI
        serializedObject.Update();
        objNameProp.stringValue = go.name;
        EditorGUILayout.HelpBox("Storing Transform values above!", MessageType.Warning);
        posProp.vector3Value = go.transform.position;
        rotProp.quaternionValue = go.transform.rotation;
        scaleProp.vector3Value = go.transform.localScale;
        //-----------------CUSTOM GUI---------------
        matIndex = EditorGUILayout.Popup("Material Type:", matIndex, matOptions);
        materialProp.enumValueIndex = matIndex;
        //Help message
        EditorGUILayout.HelpBox("Set to 100 to guarantee spawn", MessageType.Info);
        //Show our randomization factor as a slider and progress bar.
        EditorGUILayout.IntSlider(randomFactorProp, 0, 100, GUIContent.none);
        //Only show if all selected objects have same value
        if(!randomFactorProp.hasMultipleDifferentValues)
            ProgressBar(randomFactorProp.intValue/ 100.0f , "Random Chance Amount");

        serializedObject.ApplyModifiedProperties();
        //----------DEFAULT INSPECTOR-----------------
        //DrawDefaultInspector();
    }

    private static void ProgressBar(float value, string label)
    {
        //get our progress bar rect using text field margins
        Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField");
        EditorGUI.ProgressBar(rect, value, label);
        EditorGUILayout.Space();
    }
}
