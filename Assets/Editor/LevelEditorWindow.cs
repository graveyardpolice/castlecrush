﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using UnityEditor;
using UnityEngine;

[SuppressMessage("ReSharper", "UnusedMember.Local")]
public class LevelEditorWindow : EditorWindow {
    
    private int _worldNum;
    private int _levelNum;
    private string _fileName;
    private readonly string _filePath = Application.dataPath + "/Resources/GPCC";
    private const string PrefabPath = "Prefabs/";

    [MenuItem ("Graveyard Patrol/Level Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(LevelEditorWindow));
    }

    void OnGUI()
    {
        //Level Selection
        GUILayout.Label("Select World and Level to Save/Load", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        _worldNum = EditorGUILayout.IntField("World #", _worldNum);
        _levelNum = EditorGUILayout.IntField("Level #", _levelNum);
        GUILayout.EndHorizontal();
        //Set our filename after we change the values to make sure that it knows the right file to use.
        _fileName = "GPCC" + _worldNum + "-" + _levelNum + ".bytes";
        
        
        //Saving Section
        GUILayout.Label("Saving Levels", EditorStyles.boldLabel);
        if (GUILayout.Button("Save Scene to BSON"))
            SaveBson();

        //Loading Section
        GUILayout.Label("Loading Levels", EditorStyles.boldLabel);
        if (GUILayout.Button("Load Scene From BSON"))
            LoadBson();

        //Clear Scene
        GUILayout.Label("Scene Utilites", EditorStyles.boldLabel);
        if (GUILayout.Button("Empty Objects from Scene"))
            EmptyScene();
    }

    private void EmptyScene()
    {
        GameObject[] goArray = FindObjectsOfType<GameObject>();
        foreach (var obj in goArray)
            DestroyImmediate(obj);
    }

    private void LoadBson()
    {
        byte[] serialized = File.ReadAllBytes(Path.Combine(_filePath, _fileName));
        LevelObject[] unloadedObjects;
        using (MemoryStream stream = new MemoryStream(serialized))
        {
            using (BsonReader reader = new BsonReader(stream))
            {
                reader.ReadRootValueAsArray = true;
                JsonSerializer s = new JsonSerializer();
                unloadedObjects = s.Deserialize<LevelObject[]>(reader);
            }
        }
        
        GameObject[] existing = FindObjectsOfType<GameObject>();
        if(existing.Length > 0)
            foreach (GameObject go in existing)
                DestroyImmediate(go);

        foreach (LevelObject obj in unloadedObjects)
        {
            GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>(PrefabPath + obj.prefabName));
            go.transform.position = obj.position;
            go.transform.rotation = obj.rot;
            go.transform.localScale = obj.scale;
        }
    }

    private void SaveBson()
    {
        Debug.Log("Grabbing all objects in scene...");
        List<LevelObject> objectsToSave = new List<LevelObject>();      //List of simple classes to store.
        var goArray = FindObjectsOfType<GameObject>();                  //List of all objects in scene.
        List<GameObject> objectsToDestroy = new List<GameObject>();     //List of all objects we want to clear and test load.
        foreach (var go in goArray)
        {
            if (go.activeInHierarchy)
            {
                if (go.GetComponent<LevelMonoObject>() == null) 
                    continue;
                Debug.Log(go.name + " is an Active LevelObject - Adding to BSON");
                var obj = new LevelObject();
                objectsToDestroy.Add((go));
                LevelMonoObject mObj = go.GetComponent<LevelMonoObject>();
                mObj.PackObject();
                obj.prefabName = mObj.prefabName;
                obj.objectMaterial = mObj.objectMaterial;
                obj.position = mObj.position;
                obj.rot = mObj.rot;
                obj.scale = mObj.scale;
                obj.randomizeFactor = mObj.randomizeFactor;
                objectsToSave.Add(obj);
            }
        }
        Debug.Log(String.Format("Saving {0} objects to file....", objectsToSave.Count));
        if (EditorUtility.DisplayDialog("Save Scene? (Recommended)",
            "Would you like to save the scene?", "Save", "Cancel"))
            EditorApplication.SaveScene("/LevelEditorPlayground/");
        LevelObject[] saving = objectsToSave.ToArray();
        byte[] serializedBytes;
        using (var stream = new MemoryStream())
        {
            using (BsonWriter writer = new BsonWriter(stream))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, saving);
            }
            serializedBytes = stream.ToArray();
        }
        if (File.Exists(Path.Combine(_filePath, _fileName)))
        {
            if (!EditorUtility.DisplayDialog("Overwrite File?",
                    "WARNING: This file exists! Do you want to overwrite it?", "Yes", "No"))
                return;
            FinalizeSave(objectsToDestroy, serializedBytes);
        }
        else
            FinalizeSave(objectsToDestroy, serializedBytes);
    }

    private void FinalizeSave(List<GameObject> objects, byte[] bytesToSave)
    {
        //Write the bytes we want to save to file.
        File.WriteAllBytes(Path.Combine(_filePath, _fileName), bytesToSave);
        Debug.LogWarning("Overwrote file at: " + Path.Combine(_filePath, _fileName));
        //Clear out all objects in scene before we attempt a test load
        foreach (GameObject obj in objects)
            DestroyImmediate(obj);
        //Do a test load of the level
        LoadBson();
    }
}
