﻿using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour
{

    #region Variables
    //public

    //private
    private Touch firstTouch;
    #endregion

    #region Singleton
    public static TouchManager Instance
    {
        get;
        private set;
    }
    #endregion

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {


#if UNITY_ANDROID && !UNITY_EDITOR
        if (Input.touchCount > 0)
        {
            //cache our first finger to touch
            firstTouch = Input.GetTouch(0);
            //Let our listeners know we touched the screen.
            if (firstTouch.phase == TouchPhase.Began)
                EventManager.TouchBegan();
            //Send our delta movement to anyone who cares.
            if (firstTouch.phase == TouchPhase.Moved)
                EventManager.TouchMoved(firstTouch.deltaPosition);
            //Let our listeners know we ended our touch.
            if (firstTouch.phase == TouchPhase.Ended)
                EventManager.TouchEnded();
            //If the system cancelled our touch for somereason let our objects know so they can reset.
            if (firstTouch.phase == TouchPhase.Canceled)
                EventManager.TouchCancelled();
        }
#endif
    }
}
