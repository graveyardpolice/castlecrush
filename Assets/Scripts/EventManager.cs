﻿using UnityEngine;
using System.Collections;
using System;

public class EventManager : MonoBehaviour
{
    #region Events

    public static event Action<Vector2> onTouchMoved;
    public static event Action onTouchDown;
    public static event Action onTouchEnd;
    public static event Action onTouchCancel;
    
    #endregion

    public static void TouchBegan()
    {
        if(onTouchDown != null)
            onTouchDown();
    }

    public static void TouchMoved(Vector2 deltaMovement)
    {
        if (onTouchMoved != null)
            onTouchMoved(deltaMovement);
    }

    public static void TouchEnded()
    {
        if (onTouchEnd != null)
            onTouchEnd();
    }

    public static void TouchCancelled()
    {
        if (onTouchCancel != null)
            onTouchCancel();
    }
}
