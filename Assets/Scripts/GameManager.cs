﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public int totalScore;
    private int scoreExponent;
    public Text score;
    public GameObject basicAmmo;
    public GameObject[] ammoDatabase;

	public static GameManager Instance
    {
        get;
        private set;
    }
    
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;

        DontDestroyOnLoad(gameObject);
    }

	void Start () 
    {
        ResetScoreCalc();
	}
	
	// Update is called once per frame
	void Update () 
    {
        score.text = String.Format("Score: {0}", totalScore);
	}

    public void ResetScoreCalc()
    {
        scoreExponent = 0;
    }

    //Fibonnaci sequence.
    public void ScoreAdd()
    {
        scoreExponent++;
        totalScore += Mathf.FloorToInt(Mathf.Pow(scoreExponent, 2) / 8);
    }
}
