﻿using UnityEngine;

public class LevelMonoObject : MonoBehaviour {

    public string prefabName;
    public MaterialType objectMaterial; //What is our object made of?
    public int randomizeFactor; //Set to 100 to make object permanent
    public Vector3 position;
    public Vector3 scale;
    public Quaternion rot;

    public void PackObject()
    {
        position = transform.position;
        scale = transform.localScale;
        rot = transform.rotation;
    }
}
