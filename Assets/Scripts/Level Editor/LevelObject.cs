﻿using UnityEngine;

public enum MaterialType
{
    Wood = 0,
    Stone = 1,
    Metal = 2
}

public class LevelObject{
   
    public string prefabName;
    public MaterialType objectMaterial; //What is our object made of?
    public int randomizeFactor; //Set to 100 to make object permanent
    public Vector3 position;
    public Vector3 scale;
    public Quaternion rot;

}

public class PackedLevel
{
    public LevelObject[] packedObjects;
}
