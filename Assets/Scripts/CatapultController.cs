﻿using UnityEngine;

public class CatapultController : MonoBehaviour
{

    #region Variables
    //public
    public GameObject armJoint;
    public GameObject bodyPivot;
    public GameObject origPos;
    public GameObject bucketObject;
    public GameObject ammo;
    public float armPivotSmoothing;
    public float bodyPivotSmoothing;
    public float returnSpeed;
    public float fireSpeed;
    public int costToReload;
    public LayerMask CataLayer;
    
    //private
    private Transform m_trans;
    private bool releaseAmmo;
    public bool canFire;
    private Touch firstTouch;
    private bool releaseable;

    #endregion
    public Transform trans
    {
        get
        {
            if (m_trans == null)
                m_trans = GetComponent<Transform>();
            return m_trans;
        }
    }
    public Transform armTrans
    {
        get
        {
            return armJoint.transform;
        }
    }
    #region Awake
    void Awake()
    {
        ammo = (GameObject)Instantiate(GameManager.Instance.basicAmmo);
        ammo.transform.position = bucketObject.transform.position;
        ammo.rigidbody.isKinematic = true;
        releaseAmmo = false;
        canFire = true;
    }
    #endregion

    void Update() 
    {
        float zRot = armTrans.localEulerAngles.z;
        float yRot = bodyPivot.transform.localEulerAngles.y;

#if UNITY_EDITOR || UNITY_WEBPLAYER
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ResetCatapult();
            GameManager.Instance.ResetScoreCalc();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.LoadLevel(Application.loadedLevel);
        if (Input.GetMouseButton(0) && canFire)
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Debug.Log("hit1");
                Debug.Log(LayerMask.LayerToName(hit.collider.gameObject.layer));
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Catapult"))
                {
                    Debug.Log("hit2");
                    
                    releaseable = true;
                }
            }
        }
        if(releaseable)
        {
            zRot += -Input.GetAxis("Mouse Y") * armPivotSmoothing;
            yRot += Input.GetAxis("Mouse X") * bodyPivotSmoothing;
        }
        if (Input.GetMouseButtonUp(0) && canFire && releaseable)
                FireCatapult();
#endif
#if !UNITY_EDITOR && !UNITY_WEBPLAYER
        if (Input.touchCount > 0)
        {
            firstTouch = Input.GetTouch(0);
            Ray ray = Camera.main.ScreenPointToRay(firstTouch.position);
            if (firstTouch.phase == TouchPhase.Began)
            {
                ResetCatapult();
                GameManager.Instance.ResetScoreCalc();
            }
            if (firstTouch.phase == TouchPhase.Moved)
            {
                zRot += firstTouch.deltaPosition.y * armPivotSmoothing;
                yRot += firstTouch.deltaPosition.x * bodyPivotSmoothing;
            }
            if (firstTouch.phase == TouchPhase.Ended && canFire)
                FireCatapult();
        }
#endif
        zRot = Mathf.Clamp(zRot, 296, 350);
        armTrans.localRotation = Quaternion.Euler(0, 0, zRot);
        bodyPivot.transform.rotation = Quaternion.Euler(0, yRot, 0);

        if (!releaseAmmo)
            ammo.transform.position = bucketObject.transform.position;
        if (releaseAmmo)
        {
            armTrans.localRotation = Quaternion.Slerp(armTrans.localRotation, Quaternion.Euler(0, 0, 296), Time.deltaTime * returnSpeed);
        }
    }

    public void RandomAmmo()
    {
        GameObject ammoToLoad = GameManager.Instance.ammoDatabase[Random.Range(0, GameManager.Instance.ammoDatabase.Length)];
        ammoToLoad = (GameObject)Instantiate(ammoToLoad);
        ammo.SetActive(false);
        ammo = ammoToLoad;
        ammo.transform.position = bucketObject.transform.position;
        ammo.rigidbody.isKinematic = true;
        releaseAmmo = false;
        canFire = true;
    }

    private void FireCatapult()
    {
        releaseable = false;
        releaseAmmo = true;
        canFire = false;
        var shootVector = origPos.transform.position - bucketObject.transform.position;
        ammo.rigidbody.isKinematic = false;
        ammo.rigidbody.AddForce(shootVector * fireSpeed, ForceMode.Impulse);
    }

    private void ResetCatapult()
    {
        Destroy(ammo);
        ammo = (GameObject) Instantiate(GameManager.Instance.basicAmmo);
        releaseAmmo = false;
        canFire = true;
        ammo.rigidbody.isKinematic = true;
        armTrans.localRotation = Quaternion.Euler(0, 0, 296);
    }
}
