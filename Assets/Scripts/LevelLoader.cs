﻿using UnityEngine;
using System.Collections;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

public class LevelLoader : MonoBehaviour
{

    public string path;
    public string fileToLoad;
    public int levelNum;
    public string prefabPath;
    private static int LevelToLoad = 1;

    // Use this for initialization
    private void Awake()
    {
//#if UNITY_ANDROID || UNITY_IPHONE
//        path = Application.persistentDataPath + "/Resources/GPCC";
//        prefabPath = Application.persistentDataPath + "/Resources/Prefabs";
//        fileToLoad = "gpcc1-1.gpcc"; //TODO: make this dynamic
//#elif UNITY_EDITOR
	    path = Application.dataPath + "/Resources/GPCC";
        prefabPath = "Prefabs/";
//#endif
        
        LoadBson();
        DontDestroyOnLoad(this.gameObject);
    }

    private void LoadBson()
    {
        TextAsset ta = Resources.Load<TextAsset>("GPCC/GPCC1-" + LevelToLoad);
        byte[] serialized = ta.bytes;
        LevelObject[] unloadedObjects;
        using (MemoryStream stream = new MemoryStream(serialized))
        {
            using (BsonReader reader = new BsonReader(stream))
            {
                reader.ReadRootValueAsArray = true;
                JsonSerializer s = new JsonSerializer();
                unloadedObjects = s.Deserialize<LevelObject[]>(reader);
            }
        }

        foreach (LevelObject obj in unloadedObjects)
        {
            Debug.Log(obj.prefabName);
            GameObject go = (GameObject) Instantiate(Resources.Load<GameObject>(prefabPath + obj.prefabName));
            go.transform.position = obj.position;
            go.transform.rotation = obj.rot;
            go.transform.localScale = obj.scale;
            FracturedObject frac = go.GetComponent<FracturedObject>();
            if (frac != null)
                frac.EventDetachCollisionCallGameObject = gameObject;
        }
    }

    public void LoadNextLevel()
    {
        GameObject[] goList = FindObjectsOfType<GameObject>();
        foreach (GameObject go in goList)
        {
            if (go.name.Contains("Prefab"))
                Destroy(go);
        }
        LevelToLoad++;
        if (LevelToLoad >= 7)
            LevelToLoad = 1;
        LoadBson();
    }

}
